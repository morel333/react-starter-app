# README #

Starter react + router + redux project. Not all lib's might be cleared in package.json, since it's a fork from my big project. 

## Usage ##

### Init ###

``` bash
npm install
```

### Run devel ###

In 2 command lines
``` bash
npm run webpack-devserver
npm run dev  
```
If app is not rendering, in some cases debug in browser is required
then dev-client (NO-SSR mode) should be run instead of dev
``` bash
npm run webpack-devserver
npm run dev-client  
```
