global.Promise         = require('bluebird');

const isDev = process.env.NODE_ENV !== 'production';
const webpack            = require('webpack');
const path               = require('path');
const ExtractTextPlugin  = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const autoprefixer = require('autoprefixer');
const CompressionPlugin = require('compression-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

var publicPath         = '/public/assets/';
const cssName            = !isDev ? 'styles-[hash].css' : 'styles.css';
const jsName             = !isDev ? 'bundle-[hash].js' : 'bundle.js';

if (isDev) {
    publicPath = 'http://localhost:8058/public/assets';
}

const plugins = [
    new webpack.DefinePlugin({
        'process.env': {
            BROWSER:  JSON.stringify(true),
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development')
        }
    }),
    new webpack.LoaderOptionsPlugin({
        options: {
            postcss: [
                autoprefixer({
                    browsers: [
                        'last 3 version',
                        'ie >= 10'
                    ]
                })
            ],
            eslint: {
                configFile: '.eslintrc'
            },
            minimize: true
        },
        debug: isDev
    }),
    new ExtractTextPlugin(cssName),
    new CompressionPlugin({
        asset: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.(js|html)$/,
        threshold: 10240,
        minRatio: 0.8
    }),
    new CopyWebpackPlugin([
        //{ from: './src/main/webapp/favicon.ico', to: 'favicon.ico' },
        //{ from: './src/main/webapp/robots.txt', to: 'robots.txt' }
    ])
];

if (!isDev) {
    plugins.push(
        new CleanWebpackPlugin([ 'public/assets/' ], {
            root: __dirname,
            verbose: true,
            dry: false
        })
    );
    plugins.push(new webpack.optimize.OccurrenceOrderPlugin());
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        comments: false,
        compress: {
            sequences     : true,
            booleans      : true,
            loops         : true,
            unused      : true,
            warnings    : false,
            drop_console: true,
            unsafe      : true
        }
    })); // minify everything
    plugins.push(
        new ManifestPlugin({
            fileName: 'build-manifest.json'
        })
    ); // make build manifest file
}

module.exports = {
    entry: ['babel-polyfill', './src/client.js'],
    resolve: {
        modules: [
            path.join(__dirname, 'src'),
            'node_modules'
        ],
        extensions:         ['.js', '.jsx'],
        // Leaflet image Alias resolutions
        alias: {
            //'./images/layers.png$': path.resolve(__dirname, './node_modules/leaflet/dist/images/layers.png')
        }
    },
    plugins,
    output: {
        path: `${__dirname}/public/assets/`,
        filename: jsName,
        publicPath
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader'
                            /* options: {
                                sourceMap: true,
                                //modules: true,
                                localIdentName: '[name]__[local]--[hash:base64:5]'
                            } */
                        },
                        {
                            loader:'postcss-loader'
                        }
                    ]
                })
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'less-loader']
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.gif$/,
                use: [ 'url-loader?limit=10000&mimetype=image/gif' ]
            },
            {
                test: /\.jpg$/,
                use: [ 'url-loader?limit=10000&mimetype=image/jpg' ]
            },
            {
                test: /\.png$/,
                use: [ 'url-loader?limit=10000&mimetype=image/png' ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000'
            },
            {
                test: /\.jsx?$/,
                use: isDev ? ['react-hot-loader', 'babel-loader', 'eslint-loader'] : [ 'babel-loader' ],
                exclude: [/node_modules/, /public/]
            },
            {
                test: /\.json$/,
                use: [ 'json-loader' ]
            }
        ],
        noParse: /object-hash\/dist\/object_hash.js/
    },
    devtool: isDev ? 'source-map' : false,
    devServer: {
        headers: { 'Access-Control-Allow-Origin': '*' }
    }
};
