import React from 'react';
import { IndexRoute, Route }  from 'react-router';

import { AppContainer } from 'containers';
import { PageNotFound } from 'pages/Error';
import { Page1, Page2 } from 'pages/Dummy';

export default function getRoutes(store) { //eslint-disable-line
    return (
        <Route component={AppContainer}
               path='/'
        >
            <IndexRoute component={Page1}/>
            <Route component={Page2}
                   path='page2'
            />
            <Route component={PageNotFound}
                   path='*'
            />
        </Route>
    );
}
