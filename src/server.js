import express  from 'express';
import config from './config';
import path from 'path';
// --- Server ---
const app = express();
const PORT = config.port;
const ISDEV = process.env.NODE_ENV !== 'production';

import { readFileSync } from 'jsonfile';

// Disable powerby header
app.disable('x-powered-by');
// Logging middleware
const morgan = require('morgan');

if (ISDEV) {
    app.use(morgan('dev'));
} else {
    app.use(morgan('tiny'));
}

// Proxy to API server on dev env
import httpProxy from 'http-proxy';

import cookieParser from 'cookie-parser';
app.use(cookieParser());

// Api proxy
const targetUrl = `http://${config.apiHost}:${config.apiPort}`;
const proxy = httpProxy.createProxyServer({
    target: targetUrl
});

// Proxy /api to fake API
app.use('/api', (req, res) => {
    /* if (req.get('cookie')) {
     request.set('cookie', req.get('cookie'));
     } */
    // proxy cart cookie
    /* if (req.cookies.cart) {
        console.log(`-----------------> DEBUG ${req.cookies.cart}`);
    } */
    proxy.web(req, res, {
        target: `${targetUrl}/api`
    });
});

// Proxy /auth to fake API
app.use('/auth', (req, res) => {
    /* if (req.get('cookie')) {
     request.set('cookie', req.get('cookie'));
     } */
    proxy.web(req, res, {
        target: `${targetUrl}/auth`
    });
});

// Fake Resources on dev environment
if (ISDEV) {
    // fake api /resource/img folder, uses image no-image.jpg
    app.use('/resource/img', express.static(path.join(__dirname, '..', 'fakeres')));
    app.use('/resource/css', express.static(path.join(__dirname, '..', 'fakeres')));

    // fake api /resource folder
    app.use('/resource', (req, res) => {
        res.writeHead(404);
        setTimeout(() => {
            res.end();
        }, 200);
    });
}

// added the error handling to avoid https://github.com/nodejitsu/node-http-proxy/issues/527
proxy.on('error', (error, req, res) => {
    if (error.code !== 'ECONNRESET') {
        console.error('proxy error', error);
    }
    if (!res.headersSent) {
        res.writeHead(500, {
            'content-type': 'application/json'
        });
    }
    const json = {
        error: 'proxy_error', reason: error.message
    };

    res.end(JSON.stringify(json));
});

// --- Static resources
if (!ISDEV) {
    const expressStaticGzip = require('express-static-gzip');

    // app.use('/assets', express.static(path.join(__dirname, '..', '/public/assets')));
    app.use('/public/assets', expressStaticGzip(path.join(__dirname, '..', '/public/assets'), {
        enableBrotli: true,
        customCompressions: [
            {
                encodingName: 'gzip',
                fileExtension: 'gz'
            }
        ]
    }));
}

// --- Favicon ---
import favicon from 'serve-favicon';
app.use(favicon(path.join(__dirname, '..', 'static', 'favicon.ico')));

// --- Application ---

import React    from 'react';
import ReactDom from 'react-dom/server';

import { match } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import createHistory from 'react-router/lib/createMemoryHistory';
import getRoutes from './routes';

import { AppHtml, ErrorHtml } from 'utils/HTML';
import { PageNotFound } from 'pages/Error';

import { Provider } from 'react-redux';
import { ReduxAsyncConnect, loadOnServer } from 'redux-connect';
import configureStore from './redux/configureStore';

import { ReduxApiClient } from 'utils/apiclient';

app.use((req, res) => {
    const memoryHistory = createHistory(req.originalUrl);
    const store = configureStore(memoryHistory, ReduxApiClient(true, req.cookies), false);
    const history = syncHistoryWithStore(memoryHistory, store);
    let mainJs;
    let mainCss;
    let assetUrl;

    if (ISDEV) {
        mainJs = 'bundle.js';
        mainCss = 'styles.css';
        assetUrl = 'http://localhost:8058/public';
    } else {
        const manifestPath = path.join(__dirname, '..', '/public/assets/build-manifest.json');
        const manifest = readFileSync(manifestPath);

        mainJs = manifest['main.js'];
        mainCss = manifest['main.css'];
        assetUrl = '/public';
    }

    const assets = {
        'javascript': {
            'main': `${assetUrl}/assets/${mainJs}`
        },
        'css': {
            'main': `${assetUrl}/assets/${mainCss}`
        }
    };

    function hydrateOnClient() {
        res.send('<!doctype html>\n' +
            ReactDom.renderToString(
                <AppHtml store={store}
                         assets={assets}
                />
            ));
    }

    function redirectOnServer(to) {
        console.log('redirectOnServer hit');
        if (to) {
            return res.redirect(to);
        }
        return res.redirect('/');
    }

    if (config.disableSSR) {
        return hydrateOnClient();
    }

    return match({ history, routes: getRoutes(store), location: req.originalUrl }, (error, redirectLocation, renderProps) => {
            let component;

            if (redirectLocation) { // if redirect is required
                return res.redirect(301, redirectLocation.pathname + redirectLocation.search);
            } else if (error) { // Any kind of error
                return res.status(500).send(error.message);
            } else if (renderProps) {
                // if the current route matched we have renderProps

                loadOnServer({ ...renderProps, store, helpers: { ReduxApiClient, redirectOnServer } }).then(() => {
                    component = (
                        <Provider store={store} key='provider'>
                            <ReduxAsyncConnect {...renderProps} />
                        </Provider>
                    );
                    res.status(200);
                    res.send('<!doctype html>\n' + ReactDom.renderToString(<AppHtml component={component} store={store} assets={assets} />));
                }).catch((err) => {
                    console.log('Error happened');
                    console.log(err);
                    hydrateOnClient();
                    return;
                });
            } else {
                // otherwise we render a 404 page
                component = <PageNotFound/>;
                res.status(404);
                res.send(`<!doctype html>\n${
                    ReactDom.renderToString(
                        <AppHtml component={component}
                                 store={store}
                                 assets={assets}
                        />)
                    }`);
            }
        });
});

app.listen(PORT, err => {
    if (err) {
        return console.error(err);
    }
    console.log(`SERVER ==> Server listening on: ${PORT}`);
});
