import { applyMiddleware, createStore, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import DevTools from 'utils/DevTools';
import rootReducer from './modules/index';
import { routerMiddleware } from 'react-router-redux';
import { multiClientMiddleware } from 'redux-axios-middleware';
import logger from 'redux-logger';

export default function (history, clients, isClient, initialState = {}) {
    const middleware = [multiClientMiddleware(clients), routerMiddleware(history), ReduxThunk];
    // const middleware = [multiClientMiddleware(clients), ReduxThunk];

    if (isClient) {
        middleware.push(logger);
    }

    let store;
    
    if (isClient) {
        store = createStore(rootReducer, initialState, compose(
            applyMiddleware(...middleware),
            window.devToolsExtension ? window.devToolsExtension() : DevTools.instrument()
        ));
    } else {
        store = createStore(rootReducer, initialState, compose(
            applyMiddleware(...middleware),
            DevTools.instrument()
        ));
    }

    if (module.hot) {
        module.hot.accept('./modules', () =>
        store.replaceReducer(require('./modules').default)
    );
    }

    return store;
}
