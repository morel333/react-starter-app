export const RESPONSE_ERROR = 'api/RESPONSE_ERROR';
const REQUEST = 'api/REQUEST';
const RESPONSE = 'api/RESPONSE';

const initialState = {

};

/**
 * Pass through reducer to use redux-axios client
 * @param state
 * @param action
 */
export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case REQUEST:
            return {
                ...state
            };
        case RESPONSE:
            return {
                ...state
            };
        case RESPONSE_ERROR:
            return {
                ...state
            };
        default:
            return state;
    }
}

export function getRequest(url) {
    return {
        types: [REQUEST, RESPONSE, RESPONSE_ERROR],
        payload: {
            request: {
                url
            }
        }
    };
}

export function postRequest(url) {
    return {
        types: [REQUEST, RESPONSE, RESPONSE_ERROR],
        payload: {
            request: {
                method: 'POST',
                url
            }
        }
    };
}
