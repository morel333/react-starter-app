export const SET_REDIRECT_URL = 'auth/SET_REDIRECT_URL';

export const SET_USER = 'auth/SET_USER';
export const UNSET_AUTH = 'auth/UNSET_AUTH';

export const SIGN_IN_OK = 'auth/SIGN_IN_OK';
export const SIGN_IN_FAILURE = 'auth/SIGN_IN_FAILURE';
export const SIGN_IN_SEND = 'auth/SIGN_IN_SEND';

export const SIGN_OUT = 'auth/SIGN_OUT';

export const LOAD_AUTH = 'auth/LOAD_AUTH';
export const LOAD_AUTH_SUCCESS = 'auth/LOAD_AUTH_SUCCESS';
export const LOAD_AUTH_FAIL = 'auth/LOAD_AUTH_FAIL';

const initialState = {
    loaded: false,
    loggedIn: false
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case SET_REDIRECT_URL:
            return {
                ...state,
                redirectUrl: action.url
            };
        case SET_USER:
            return {
                ...state,
                user: action.user,
                loggedIn: true
            };
        case UNSET_AUTH:
            return {
                ...state,
                user: null,
                loggedIn: false,
                token: null,
                error: null
            };
        case SIGN_IN_SEND:
            return {
                ...state,
                loggingIn: true,
                error: null
            };
        case SIGN_IN_OK:
            return {
                ...state,
                loggingIn: false,
                error: null,
                token: action.payload.data.token
            };
        case SIGN_IN_FAILURE:
            return {
                ...state,
                loggingIn: false,
                loggedIn: false,
                error: action.error
            };
        case SIGN_OUT:
            return {
                ...state,
                loggedIn: false,
                token: null,
                user: null
            };
        case LOAD_AUTH:
            return {
                ...state,
                loading: true
            };
        case LOAD_AUTH_SUCCESS:
            return {
                ...state,
                loaded:true,
                loading: false,
                token: action.payload.data.token,
                loggedIn: true
            };
        case LOAD_AUTH_FAIL:
            return {
                ...state,
                loaded: true,
                loading: false,
                user: null,
                loggedIn: false,
                token: null,
                error: null
            };
        default:
            return state;
    }
}

export function setRedirectUrl(currentURL) {
    return {
        type: SET_REDIRECT_URL,
        url: currentURL
    };
}

export function setUser(user) {
    return {
        type: SET_USER,
        user
    };
}

export function unsetAuth() {
    return {
        type: UNSET_AUTH
    };
}

function login(url, data) {
    return {
        types: [SIGN_IN_SEND, SIGN_IN_OK, SIGN_IN_FAILURE],
        payload: {
            request: {
                method: 'post',
                url,
                data
            }
        }
    };
}

export function emailSignIn(email, password) {
    const data = {
        login: email,
        password
    };

    return login('/auth/login', data);
}

export function isLoaded(state) {
    return state.auth && state.auth.loaded;
}

export function loadAuth() {
    return {
        types: [LOAD_AUTH, LOAD_AUTH_SUCCESS, LOAD_AUTH_FAIL],
        payload: {
            request: {
                method: 'get',
                url: '/auth/authorize'
            }
        }
    };
}

/**
 * Method for SSR BEFORE app, asyncLoad etc. Need's a dispatch
 * This is not a pure ACTION, can be refactored // TODO move me to server.js
 */
export function loadAuthOnServer() {
    return (dispatch, getState) => {
        if (!isLoaded(getState())) {
            return dispatch(loadAuth())
                .then((result) => {
                    if (result.payload && result.payload.status === 200) {
                        try {
                            dispatch(setUser(result.payload.data.token.user));
                        } catch (err) {
                            dispatch(unsetAuth());
                        }
                    }
                });
        }
        return Promise.resolve();
    };
}

export function signOut() {
    return {
        types: [SIGN_OUT, SIGN_OUT, SIGN_OUT],
        payload: {
            request: {
                method: 'get',
                url: '/auth/logout'
            }
        }
    };
}
