import { routerReducer } from 'react-router-redux';
import { reducer as reduxAsyncConnect } from 'redux-connect';
import { combineReducers } from 'redux';

import api from './api';
import auth from './auth/auth';

export default combineReducers({
    reduxAsyncConnect,
    api,
    auth,
    routing: routerReducer
});
