export const GLOBAL_RESET = 'global/GLOBAL_RESET';

export function globalReset() {
    return {
        type: GLOBAL_RESET
    };
}
