import { applyMiddleware, createStore } from 'redux';
import ReduxThunk from 'redux-thunk';
import rootReducer from './modules/index';
import { multiClientMiddleware } from 'redux-axios-middleware';
import { routerMiddleware } from 'react-router-redux';

export default function (history, clients, isClient, initialState = {}) {
    const middleware = [multiClientMiddleware(clients), routerMiddleware(history), ReduxThunk];
    // const middleware = [multiClientMiddleware(clients), ReduxThunk];
    
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(...middleware)
    );
}
