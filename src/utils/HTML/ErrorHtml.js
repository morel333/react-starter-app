import React, {Component} from 'react';

class ErrorHtml extends Component {
    render() {
        const content = 'Error happened, try again later';

        return (
            <html lang='en-us'>
                <head>
                    <meta charSet='utf-8' />
                    <meta name='viewport' content='width=device-width, initial-scale=1' />
                    <title>Error</title>
                </head>
                <body>
                    <div id='main' dangerouslySetInnerHTML={{__html: content}}/>
                </body>
            </html>
        );
    }
}

export default ErrorHtml;
