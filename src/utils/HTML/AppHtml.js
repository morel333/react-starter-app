import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom/server';
import CircularJSON from 'circular-json';

class AppHtml extends Component {

    render() {
        const { component, store, assets } = this.props;
        const content = component ? ReactDOM.renderToString(component) : '';
        return (
            <html lang='en-us'>
                <head>
                    <meta charSet='utf-8' />
                    <meta name='viewport' content='width=device-width, initial-scale=1' />
                    <title>Hello</title>
                    <link rel='stylesheet' href={assets.css.main}/>
                    <script dangerouslySetInnerHTML={{__html: `window.__data=${CircularJSON.stringify(store.getState())};`}} charSet='UTF-8'/>
                </head>
                <body>
                    <div id='root' dangerouslySetInnerHTML={{__html: content}}/>
                    <script src={assets.javascript.main} charSet='UTF-8'/>
                </body>
            </html>
        );
    }
}

AppHtml.propTypes = {
    assets: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object
};

export default AppHtml;
