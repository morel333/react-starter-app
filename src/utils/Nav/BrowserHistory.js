import { browserHistory } from 'react-router';

export function navigateTo(e, path = '/') {
    e.preventDefault();
    browserHistory.push(path);
}

export function replaceTo(e, path = '/') {
    e.preventDefault();
    browserHistory.replace(path);
}
