/**
 * axios based error message parser
 * @param error
 */
export default function responseErrorParser(error) {
    /* eslint-disable */
    if (error) {
        const message = error.message;
        if (error.response) {
            // const status = error.response.status;
            const statusText = error.response.statusText;
            const data = error.response.data;
            if (data) {
                return data;
            }
            return statusText;
        }
        return message;
    }
    return null;
    /* eslint-enable */
}
