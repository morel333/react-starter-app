import React from 'react';

export function objectsToOptions(objectsArray = [], keyVal = 'id', nameVal = 'name', doOrder = false) {
    const out = [];

    if (objectsArray && objectsArray.length > 0) {
        if (doOrder) {
            objectsArray.sort((a, b) => {
                return a.order - b.order;
            });
        }
        objectsArray.map((o, i) => { // eslint-disable-line
            out.push(
                <option
                    key={o[keyVal]}
                    value={o[keyVal]}
                >
                    {o[nameVal]}
                </option>
            );
        });
    }
    return out;
}

/**
 * Reads value of input field based on field type
 * Returns as:
 * 1) String
 * 2) Number
 * @param e
 * @returns {*}
 */
export function readInput(e) {
    if (e.target.type === 'number') {
        return Number(e.target.value);
    }
    return e.target.value;
}

/**
 * Input fields validation
 * @param value field value
 * @param validationParams validation parameters
 * @param finalCheck
 * @param defaultValue
 * @returns {object} containing: <br/>
 * state - field state for FormGroup.validationState <br/>
 * text - text. can be used to display validation problem <br/>
 * valid - boolean indicator <br/>
 */
export function inputValidation(value, validationParams, finalCheck = false, defaultValue = '') {
    const out = {
        state: null,
        text: null,
        isValid: false
    };

    // no validation params = no validation
    if (!validationParams) {
        out.isValid = true;
        return out;
    }
    // for field not to be 'red' initially, dont set state here
    if (!finalCheck && value === defaultValue) {
        // still, perform notNull if set
        if (validationParams.notEmpty) {
            if (!value || value.length === 0) {
                out.text = 'Обязательно к заполнению';
            }
        }
        return out;
    }
    if (validationParams.notEmpty) {
        if (!value || value.length === 0) {
            out.state = 'error';
            out.text = 'Обязательно к заполнению';
            return out;
        }
    }
    if (validationParams.maxLength) {
        if (value && value.length > validationParams.maxLength) {
            out.state = 'error';
            out.text = `Максимальная длинна ${validationParams.maxLength}`;
            return out;
        }
    }
    if (validationParams.minLength) {
        if (value && value.length < validationParams.minLength) {
            out.state = 'error';
            out.text = `Минимальная длинна ${validationParams.minLength}`;
            return out;
        }
    }
    if (validationParams.restrictions && validationParams.restrictions.length > 0) {
        // IMPLEMENT
    }
    if (validationParams.rules && validationParams.restrictions.rules > 0) {
        // IMPLEMENT
    }

    out.state = 'success';
    out.isValid = true;
    return out;
}

