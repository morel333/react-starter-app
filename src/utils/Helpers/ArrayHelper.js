/**
 * Find object in array to object value of given key
 * @param arr array to find in
 * @param value value to look for
 * @param key key to look in, id by default
 * @param subKey - subKey to look in if key is an object
 * @returns {*}
 */
export function findInArrByVal(arr, value, key = 'id', subKey = null) {
    if (arr && arr.length > 0) {
        for (let i = 0; i < arr.length; i++) {
            if (!subKey) {
                if (arr[i][key] === value) {
                    return arr[i];
                }
            } else {
                if (arr[i][key] && arr[i][key][subKey] && arr[i][key][subKey] === value) { // eslint-disable-line
                    return arr[i];
                }
            }
        }
    }
    return null;
}

/**
 * Find object in array
 * @param a array to find in
 * @param obj object to find
 * @returns {*}
 */
export function findInArr(a, obj) {
    for (let i = 0; i < a.length; i++) {
        if (a[i] === obj) {
            return a[i];
        }
    }
    return null;
}

/**
 * Checks if array contains value
 * @param a array to find in
 * @param obj object to find
 * @returns {boolean}
 */
export function containsInArr(a, obj) {
    return !!findInArr(a, obj);
}
