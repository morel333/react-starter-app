export default function serial(arrayOfPromises) {
    const results = [];
    
    return arrayOfPromises.reduce((seriesPromise, promise) => {
        return seriesPromise.then(() => {
            return promise
                .then((result) => {
                    results.push(result);
                });
        });
    }, Promise.resolve())
        .then(() => {
            return results;
        });
}
