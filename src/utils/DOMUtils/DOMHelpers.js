import { updateMeta } from './StarterKit';

export function updateDescription(commonDescription, locationDescription) {
    /* if (locationDescription) {
        updateMeta('description', `${commonDescription} - ${locationDescription}`);
    } else {
        updateMeta('description', `${commonDescription}`);
    } */
    if (locationDescription) {
        updateMeta('description', `${locationDescription}`);
    } else if (commonDescription) {
        updateMeta('description', `${commonDescription}`);
    }
}

export function updateKeywords(commonKeywords = [], locationKeywords = []) {
    // updateMeta('keywords', commonKeywords.concat(locationKeywords).join(','));
    if (locationKeywords && locationKeywords.length > 0) {
        updateMeta('keywords', locationKeywords.join(','));
    } else if (commonKeywords && commonKeywords.length > 0) {
        updateMeta('keywords', commonKeywords.join(','));
    }
}
