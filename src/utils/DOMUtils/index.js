import { updateMeta as replaceMeta } from './StarterKit';
import { updateDescription, updateKeywords } from './DOMHelpers';

export { replaceMeta, updateDescription, updateKeywords };
