import axios from 'axios';
import appConfig from '../../config';

/**
 * Api client to be used in conponents
 */
export default function ApiClient(__SERVER__, cookies = null) {
    let apiEndPoint = '';

    // Endpoint setup. Server axios needs full path:port
    if (__SERVER__) {
        apiEndPoint = `http://${appConfig.apiHost}:${appConfig.apiPort}`;
    }

    const deafultClientOpts = {
        baseURL: apiEndPoint,
        timeout: 10000,
        responseType: 'json'
    };

    // TODO needs inplementation
    const axiosMiddlewareCustomOptions = { // eslint-disable-line
        interceptors: {
            request: [
                ({ getState, dispatch, getSourceAction }, config) => {
                    // Interceptor setting Authorization header from token
                    const newCfg = config;

                    if (getState().auth.token) {
                        newCfg.headers.Authorization = `Bearer ${getState().auth.token}`;
                    }
                    return newCfg;
                },
                ({ getState, dispatch, getSourceAction }, config) => {
                    // Interceptor passing cookies from incoming request during SSR
                    const newCfg = config;

                    if (__SERVER__ && cookies && cookies.session) {
                        if (cookies.session) {
                            newCfg.headers.cookie = `session=${cookies.session}`;
                        }
                    }
                    return newCfg;
                }
            ]
        }
    };

    return axios.create(deafultClientOpts);
}
