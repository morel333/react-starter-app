import axios from 'axios';
import appConfig from '../../config';

/**
 * Redux api client
 * @param __SERVER__
 * @param cookies
 * @returns {{default: {client: deafultClientOpts, options: {interceptors: {request: *[]}}}}}
 * @constructor
 */
export default function ReduxApiClient(__SERVER__, cookies = null) {
    let apiEndPoint = '';

    // Endpoint setup. Server axios needs full path:port
    if (__SERVER__) {
        apiEndPoint = `http://${appConfig.apiHost}:${appConfig.apiPort}`;
    }

    const deafultClientOpts = {
        baseURL: apiEndPoint,
        timeout: 10000,
        responseType: 'json'
    };

    const axiosMiddlewareCustomOptions = {
        interceptors: {
            request: [
                ({ getState, dispatch, getSourceAction }, config) => {
                    // Interceptor setting Authorization header from token
                    const newCfg = config;

                    if (getState().auth.token) {
                        newCfg.headers.Authorization = `Bearer ${getState().auth.token}`;
                    }
                    return newCfg;
                },
                ({ getState, dispatch, getSourceAction }, config) => {
                    // Interceptor passing cookies from incoming request during SSR
                    const newCfg = config;

                    if (__SERVER__ && cookies) {
                        const cookiesSSR = [];
                        
                        if (cookies.session) {
                            cookiesSSR.push(`session=${cookies.session}`);
                        }
                        if (cookies.cart) {
                            cookiesSSR.push(`cart=${cookies.cart}`);
                        }
                        if (cookiesSSR.length > 0) {
                            newCfg.headers.cookie = cookiesSSR.join('; ');
                        }
                    }
                    return newCfg;
                }
            ]
        }
    };

    return {
        default: {
            client: axios.create(deafultClientOpts),
            options: axiosMiddlewareCustomOptions
        }
    };
}
