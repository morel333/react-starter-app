import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { asyncConnect } from 'redux-connect';

@asyncConnect([ {
    promise: ({ location, store: { dispatch, getState } }) => { // eslint-disable-line
        const promises = [];

        return Promise.all(promises);
    }
} ])
class AppContainer extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>
                <div className='main'>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

AppContainer.propTypes = {
    children: PropTypes.node
};

function mapStateToProps(state) { // eslint-disable-line
    return {

    };
}

export default connect(mapStateToProps)(AppContainer);
