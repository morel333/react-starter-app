import React from 'react';
import ReactDOM from 'react-dom';

import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import getRoutes from 'routes';

import { Provider } from 'react-redux';
import { ReduxAsyncConnect } from 'redux-connect';
import configureStore from './redux/configureStore';

import { ReduxApiClient } from 'utils/apiclient';

const initialState = window.__data || {};
const store = configureStore(browserHistory, ReduxApiClient(), true, initialState);
const history = syncHistoryWithStore(browserHistory, store);
const dest = document.getElementById('root');

ReactDOM.render(
    <Provider store={store}
              key='provider'
    >
        <Router render={(props) =>
            <ReduxAsyncConnect {...props}
                helpers={ReduxApiClient}
                filter={item => !item.deferred}
            />
            }
                history={history}
        >
            {getRoutes(store)}
        </Router>
    </Provider>,
    dest
);

if (process.env.NODE_ENV !== 'production') {
    window.React = React; // enable debugger
    if (!dest || !dest.firstChild || !dest.firstChild.attributes || !dest.firstChild.attributes['data-react-checksum']) {
        console.error('Server-side React render was discarded. Make sure that your initial render does not contain any client-side code.'); // eslint-disable-line max-len
    }
}
