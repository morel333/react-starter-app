import React, { Component } from 'react';

class Page1 extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>
                <p>Page 1</p>
            </div>
        );
    }
}

export default Page1;
