import React, { Component } from 'react';

class Page2 extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>
                <p>Page 2</p>
            </div>
        );
    }
}

export default Page2;
