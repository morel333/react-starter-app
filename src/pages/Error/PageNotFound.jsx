import React, { Component } from 'react';
import { Link } from 'react-router';
import { PageHeader } from 'component/Header';
import PropTypes from 'prop-types';

const style = {
    height: '100%',
    width: '100%',
    textAlign: 'center'
};

class PageNotFound extends Component {
    componentDidMount() {
        if (this.context.navNameOverride && !this.props.isWrappedInPage) {
            this.context.navNameOverride('Ошибка', '/404');
        }
    }

    render() {
        return (
            <div
                 style={style}
            >
                <PageHeader title='Page not found!'/>
                <p>
                    <Link to='/'>Go back to the main page</Link>
                </p>
            </div>
        );
    }
}

PageNotFound.propTypes = {
    isWrappedInPage: PropTypes.bool
};

PageNotFound.contextTypes = {
    navNameOverride: PropTypes.func
};

export default PageNotFound;
