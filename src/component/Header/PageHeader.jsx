import React from 'react';
import PropTypes from 'prop-types';

class PageHeader extends React.Component {
    render() {
        const { title, className } = this.props;

        return (
            <div className='row'>
                <div className='col'>
                    <h1 className={className}>{title}</h1>
                </div>
            </div>
        );
    }
}

PageHeader.propTypes = {
    title: PropTypes.string.isRequired,
    className: PropTypes.string
};

PageHeader.defaultProps = {
    className: 'page-header'
};

export default PageHeader;
