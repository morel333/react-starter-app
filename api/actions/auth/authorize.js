/**
 * Get cookie, lookup session from it, bring up JWT and pass in body
 * @param req
 * @returns {Promise}
 */
export function authorize(req,res) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (req.session.token) {
                resolve({'token':req.session.token} || null);
            } else {
                req.session.destroy();
                res.clearCookie('session');
                var error = {
                    status: 401,
                    error: 'Auth expired'
                };
                reject(error);
            }
        }, 100 );
    });
}
