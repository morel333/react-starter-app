import { mapUrl } from './url';

export function mapRest(req, res, availableActions = {}, urlParams = []) {
    const { action, params } = mapUrl(availableActions, urlParams);

    console.log(`API ==> REST urlParams: ${urlParams} | REST params: ${params} | taken action ==> ${action}`);
    if (action) {
        return action(req, res, params);
    }
    return new Promise((resolve, reject) => {
        reject({
            status:404,
            text:'NOT FOUND'
        });
    });
}
