import { mapUrl } from './url';
import { mapRest } from './rest';

export { mapUrl, mapRest as mapParams };
