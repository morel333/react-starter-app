import express from 'express';
import session from 'express-session';
import PrettyError from 'pretty-error';
import http from 'http';
import config from '../src/config';
import { mapUrl } from 'utils/index';
import bodyParser from 'body-parser';
import * as actions from './actions/index';
import cookieParser from 'cookie-parser';

const pretty = new PrettyError(); // eslint-disable-line no-unused-vars
const app = express();
const server = new http.Server(app); // eslint-disable-line no-unused-vars

app.disable('x-powered-by');
app.use(session({
    secret: '8some seCret keY 112#',
    resave: false,
    saveUninitialized: false,
    name: 'session',
    cookie: { maxAge: 60 * 60 * 1000 }
}));
app.use(bodyParser.json());
app.use(cookieParser());

app
    .use((req, res) => {
        const splittedUrlPath = req.url.split('?')[0].split('/').slice(1);
        const { action, params } = mapUrl(actions, splittedUrlPath);
        
        console.log(`API ---------------------------Authorization: ${req.get('Authorization')}`);
        console.log(`API ==> Incoming urlPath: ${splittedUrlPath} | API params: ${params} | taken action ==> ${action}`);
        if (action) {
            action(req, res, params)
                .then((result) => {
                    if (result instanceof Function) {
                        result(res);
                    } else {
                        res.json(result);
                    }
                }, (reason) => {
                    if (reason && reason.redirect) {
                        res.redirect(reason.redirect);
                    } else {
                        console.error('API ERROR:', reason);
                        res.status(reason.status || 500).json(reason);
                    }
                });
        } else {
            res.status(404).end('NOT FOUND');
        }
});

if (config.apiPort) {
    const runnable = app.listen(config.apiPort, (err) => { // eslint-disable-line no-unused-vars
        if (err) {
            console.error(err);
        }
        console.info('----\nAPI ==> API is running on port %s', config.apiPort);
        console.info('API ==> Send requests to http://%s:%s', config.apiHost, config.apiPort);
    });
} else {
    console.error('API ==> ERROR: No PORT environment variable has been specified');
}
